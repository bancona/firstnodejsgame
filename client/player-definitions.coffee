CARDINAL = "#C41E3A"

# Player Class
# ------------

class Player
  constructor: ->
    @reset()

  update: (numSteps) ->
    @x += @vx * numSteps
    @y += @vy * numSteps

    @image?.attr
      x: @x
      y: @y

    return

  reset: ->
    @x = @y = @vx = @vy = 0
    @width = @height = 0
    @alive = false
    @image = null
    @id = null
    return

  setup: (playerInfo) ->
    @x      = playerInfo.x      ? @x
    @y      = playerInfo.y      ? @y
    @vx     = playerInfo.vx     ? @vx
    @vy     = playerInfo.vy     ? @vy
    @width  = playerInfo.width  ? @width
    @height = playerInfo.height ? @height
    @id     = playerInfo.id     ? @id
    @image  = playerInfo.image  ? @image

    if @image?
      @image.attr
        x:      @x
        y:      @y
        width:  @width
        height: @height

      @image.show()

    # For convenience in chaining functions
    this

  kill: ->
    @alive = false
    @image?.hide()

    # For convenience in chaining functions
    this

# MainPlayer Class
# ----------------

class MainPlayer extends Player
  constructor: (@startInfo, paper) ->
    super @startInfo

    @image = paper.rect(
      @startInfo.x
      @startInfo.y
      @startInfo.width
      @startInfo.height
    ).attr "fill", CARDINAL

  reset: ->
    @x = @startInfo.x
    @y = @startInfo.y
    @vx = 0
    @vy = 0
    @width = @startInfo.width
    @height = @startInfo.height

    if @image?
      @image.attr
        x: @x
        y: @y
        width: @width
        height: @height
        fill: CARDINAL

      @image.show()

    return

  update: (numSteps, controls) ->
    @vx = @vy = 0
    moveSpeed = 2

    up =    controls.up()
    left =  controls.left()
    down =  controls.down()
    right = controls.right()

    if up != down
      if up is on
        @vy = -moveSpeed
      else
        @vy = moveSpeed

    if left != right
      if left is on
        @vx = -moveSpeed
      else
        @vx = moveSpeed

    super numSteps
    return

# Player Pool
# -----------

# Inspired by the **Object Pools** section of the book
# [HTML5 Game Development Insights](http://www.apress.com/9781430266976)
# (ISBN #9781430266976)

class PlayerPool
  constructor: (@size, @paper) ->
    @reset()

  reset: ->
    # Array of players in pool
    @players = new Array @size

    # Array of images for players in pool
    @images = new Array @size

    # Crude linked list of indices
    @freeIndices = new IndexLinkedList()

    # Fill @players with new Players and
    # fill @images with new, invisible images and
    # fill @freeIndices with all indices
    for i in [0..@size]
      @players[i] = new Player()
      @images[i] = @paper.rect(5, 5, 5, 5).attr "fill", CARDINAL
      @images[i].hide()

      @freeIndices.add i

  # Crude linked list holding free indices
  class IndexLinkedList
    constructor: ->
      @head = null

    existsFreeIndex: ->
      @head?

    add: (freeIndex) ->
      @head =
        index: freeIndex
        next: @head

    remove: ->
      return null unless @head

      node = @head
      @head = node.next

      node.index

  add: (playerID, playerInfo) ->
    return null unless @freeIndices.existsFreeIndex()

    playerInfo.id = playerID
    freeIndex = @freeIndices.remove()
    playerInfo.image = @images[freeIndex]
    player = @players[freeIndex].setup playerInfo
    player.alive = true
    player.image.show()

    player

  get: (playerID) ->
    return null unless playerID?

    for player in @players
      if player.id == playerID
        return player

    null

  update: (playerID, numSteps) ->
    for player in @players
      if player.id == playerID
        player.update numSteps
        return
    return

  updateAll: (numSteps) ->
    player.update numSteps for player in @players when player.alive
    return

  remove: (playerID) ->
    for player, i in @players
      if player.id == playerID
        player.kill()
        @freeIndices.add i
        return true

    false

  removeAll: ->
    for player, i in @players
      player.kill()
      @freeIndices.add i
    return

module.exports =
  MainPlayer: MainPlayer
  PlayerPool: PlayerPool