# Controls
# ========

# Author: Alberto Ancona,
# Date: 07/14/2015

# Key codes are stored here for convenience
keys =
  w:     87
  a:     65
  s:     83
  d:     68
  up:    38
  left:  37
  down:  40
  right: 39

# Holds status of all keys that have been pressed.
# keyStates[key] is true iff key is pressed.
keyStates = {}

# Sets all in keyStates to off
clearKeys = ->
  keyStates[key] = off for own key of keys
  return

# Sets key indicated by keyCode to state
setKeyState = (keyCode, state) ->
  for own key, code of keys
    if keyCode == code
      keyStates[key] = state
      break
  return

# Returns true if any of the keys passed are pressed
isPressed = ->
  return yes for key in arguments when keyStates[key]
  no

# Initalizes all keys in keyStates to false
clearKeys()

# Event Handlers
# --------------

# Records keydown event as a true state in keyStates
window.addEventListener "keydown", (e) ->
  setKeyState e.which, on
  return

# Records keyup event as a false state in keyStates
window.addEventListener "keyup", (e) ->
  setKeyState e.which, off
  return

# If user stops looking at the browser page/tab,
# treat all keys as being unpressed
window.addEventListener "blur", ->
  clearKeys()
  return

# API
# ---

# Controls defined here.
# Either WASD or directional controls may be used
module.exports =
  up:    -> isPressed "w", "up"
  down:  -> isPressed "s", "down"
  left:  -> isPressed "a", "left"
  right: -> isPressed "d", "right"