# Menus
# =====

module.exports.setupMainMenu = (paper, color, callback) ->
  buttonW = 150
  buttonH = 60

  addedElements = []

  onButtonPush = ->
    for el in addedElements
      el.remove()
    callback()

  addedElements.push paper.text(
    paper.canvas.offsetWidth / 2
    100
    "PLAYER DASH"
  ).attr
    "fill":         color
    "stroke":       "#ccc"
    "stroke-width": 2
    "font-size":    80
    "font-weight":  "bold"

  addedElements.concat addButton(
    paper,
    "START"
    ,
      x: (paper.canvas.offsetWidth - buttonW) / 2
      y: (0.75 * paper.canvas.offsetHeight - buttonH) / 2 + 80
      w: buttonW
      h: buttonH
      r: 10
    ,
      fill:   color
      stroke: "#ccc"
    , onButtonPush
  )

  return

addButton = (paper, text, rect, color, callback) ->
  # add button background and outline, in speciifed place and color
  rectElement = paper.rect(rect.x, rect.y, rect.w, rect.h, rect.r)
  .attr
    "fill":         color.fill
    "stroke":       color.stroke ? "#ccc"
    "stroke-width": 2

  # add text on top of button, centered on it
  textElement = paper.text(rect.x + rect.w/2, rect.y + rect.h/2, text)
  .attr
    "fill":        "#ccc"
    "font-size":   18
    "font-weight": "bold"

  # Both text and background call callback when clicked
  rectElement.node.onclick = callback
  textElement.node.onclick = callback

  # Return text and background
  [rectElement, textElement]
