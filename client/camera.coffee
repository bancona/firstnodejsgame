# Camera
# ------

camera =
  # Object that camera follows.
  # focusObj must have the fields x and y
  focusObj: null

  # Starting Coordinates for focusObj, to be used as a reference
  startX: 0
  startY: 0

  # Difference between start coordinates and current focusObj location
  dx: 0
  dy: 0

# Initialize camera with x and y values from focusObj
camera.init = (focusObj) ->
  if focusObj?
    camera.focusObj = focusObj
    camera.startX   = focusObj.x
    camera.startY   = focusObj.y

  camera.dx = camera.dy = 0

  return

# Update camera by moving view box of the canvas
# to reflect movement of focusObj
camera.update = (paper) ->
  if camera.focusObj? and paper?
    # Calculate change in location of focusObj
    camera.dx = camera.focusObj.x - camera.startX
    camera.dy = camera.focusObj.y - camera.startY

    # Move the view box of the canvas
    paper.setViewBox(
      camera.dx
      camera.dy
      paper.canvas.offsetWidth
      paper.canvas.offsetHeight
      true
    )

  return

# Expose the camera object as an export
module.exports = camera