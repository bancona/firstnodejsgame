# Game Client
# ===========

# Constants
# ---------

# Starting position for the main player
PLAYER_START_X = 200
PLAYER_START_Y = 100

# Starting size for the main player
PLAYER_START_WIDTH =  50
PLAYER_START_HEIGHT = 50

# Maximum number of players in room other than main player
MAX_NUM_OTHER_PLAYERS = 19

# Time allocated for processing for each animation frame.
# Equal to 16.67 ms, or 60 frames per second (FPS)
TIME_PER_FRAME = 1000 / 60

# Colors
SKY_COLOR = "#77f"
CARDINAL =  "#C41E3A"

# Globals
# -------

# Allows cancellation of gameLoop
gameLoopIntervalID = null

# Keeps track of how long calculations took each frame
lastRunTime = 0

# Raphael Paper object for use in drawing on canvas
paper = null

# PlayerPool and MainPlayer definitions imported from player-definitions
playerDefinitions = require "./player-definitions"
PlayerPool = playerDefinitions.PlayerPool
MainPlayer = playerDefinitions.MainPlayer

# Set players in game to null initially
mainPlayer =   null
otherPlayers = null

# Provides functions mainPlayer can use to move and perform actions
controls = require "./controls"

# Object that handles panning over the canvas, focuses on mainPlayer
camera = require "./camera"

# Game Logic:
# ----------

# Connect to server and begin calculating latency
socket = io()

# On window "load" event, initalize game and display main menu
window.addEventListener "load", ->
  # Create Raphael paper object to fill div with id "game-canvas" completely
  paper = Raphael "game-canvas", "100%", "100%"
  paper.safari()

  # Pool of players
  otherPlayers = new PlayerPool MAX_NUM_OTHER_PLAYERS, paper

  # Define how client will deal with communications from server
  require("./socket-methods").init socket, otherPlayers, paper

  # Setup main menu, with a callback to **startGame**
  require("./menus").setupMainMenu paper, CARDINAL, startGame

  return

# Function to be called at the start of the game.
# Clears the canvas, resets main player,
# informs server of new player, begins game loop
startGame = ->
  # Clear canvas
  paper.clear()

  otherPlayers.reset()

  # Reset **mainPlayer**
  if not mainPlayer?
    initialPlayerInfo =
      x:      PLAYER_START_X
      y:      PLAYER_START_Y
      width:  PLAYER_START_WIDTH
      height: PLAYER_START_HEIGHT

    mainPlayer = new MainPlayer initialPlayerInfo, paper

  mainPlayer.reset()

  # Inform server of new player
  socket.emit "new player",
    info: mainPlayer.startInfo

  socket.emit "request all players info", {}

  # Set camera to follow **mainPlayer**
  camera.init mainPlayer

  # Begin game loop
  gameLoopIntervalID = setInterval gameLoop, TIME_PER_FRAME

  return

gameLoop = ->
  start = new Date().getTime()

  oldVx = mainPlayer.vx
  oldVy = mainPlayer.vy

  numSteps = 1 #lastRunTime / TIME_PER_FRAME

  # Update mainPlayer
  mainPlayer.update numSteps, controls

  # Update other players in game (client interpolation)
  otherPlayers.updateAll numSteps

  # If mainPlayer velocity has changed,
  # send "velocity update" to server
  if true#oldVx != mainPlayer.vx or oldVy != mainPlayer.vy
    socket.emit "player update",
      info:
        x:    mainPlayer.x
        y:    mainPlayer.y
        vx:   mainPlayer.vx
        vy:   mainPlayer.vy

  # Update camera based on game changes
  #camera.update paper

  # Record time to complete gameLoop
  lastRunTime = new Date().getTime() - start

  return