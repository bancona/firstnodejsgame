module.exports.init = (socket, otherPlayers, paper, playing) ->
  playing = playing ? true

  socket.on "new player", (data) ->
    return unless playing and data.id isnt socket.id
    otherPlayers.add data.id, data.info
    null

  socket.on "player update", (data) ->
    return unless playing
    otherPlayers.get(data.id)?.setup data.info
    null

  socket.on "all players info", (allOtherPlayers) ->
    return unless playing
    for own id, info of allOtherPlayers
      if id isnt socket.id then otherPlayers.add id, info
    null

  socket.on "disconnect player", (data) ->
    return unless playing
    otherPlayers.remove data?.id
    null

  socket.on "ping", (data) ->
    socket.emit "pong", data
    null