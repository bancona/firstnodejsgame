players = {}

module.exports.players = players

latencyCalculator = require "./latency-calculator"

module.exports.init = (io) ->

  latencyCalculator.init io

  io.on "connection", (socket) ->

    if process.env.DEBUG then console.log "New Connection id=#{socket.id}"

    # "New Player" Handler
    # --------------------
    # Adds new player to **players** and informs other players of new player
    socket.on "new player", (data) ->

      players[socket.id] = data.info

      socket.broadcast.emit "new player",
        id: socket.id
        info: data.info

      null


    # "Request All Players Info" Handler
    # ----------------------------------
    # Send information about other players back to new player
    # upon request
    socket.on "request all players info", ->
      socket.emit "all players info", players


    # "Player Update" Handler
    # -----------------------
    # If player update data is valid, incorporates update info
    # into game state and informs other players of update
    socket.on "player update", (data) ->

      # Check if **player** and **playerInfo** exist; if not, return
      player = players[socket.id]
      playerInfo = data.info
      return unless player? and playerInfo?

      # If info in **playerInfo** is different than info,
      # update **players** with new information and
      # set flag **change** to true
      change = false
      for own key, value of playerInfo
        if key is "vx" or key is "vy"
          playerInfo[key.charAt(1)] +=
            value *
            latencyCalculator.getTypicalLatency(socket.id) //
            (1000//60)

        if not player[key]? or player[key] != value
          change = true
          player[key] = value

      if change
        socket.broadcast.emit "player update",
          id: socket.id
          info: playerInfo
      null


    # "Disconnect" Handler
    # --------------------
    # Informs other players of disconnect
    # and removes player from **players**
    socket.on "disconnect", ->
      socket.broadcast.emit "disconnect player", id: socket.id
      delete players[socket.id]
      null

    return