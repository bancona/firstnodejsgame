allLatencies = {}
intervalIDs = {}
typicalLatencies = {}
timeSince = (time) -> Date.now() - time

stats = require "stats-lite"

MAX_NUM_LATENCIES = 40
PING_INTERVAL = 1000

calculateTypicalLatency = (id) ->

  latencies = allLatencies[id]

  return null unless latencies?

  mean = stats.mean latencies

  return mean if latencies.length < 10

  stdev = stats.stdev latencies

  latencies =
    (lat for lat in latencies when Math.abs(lat - mean) <= stdev)

  stats.mean latencies

module.exports.init = (io) ->

  io.on "connection", (socket) ->

    allLatencies[socket.id] = []

    typicalLatencies[socket.id] = 0

    # Repeat "ping" every 1000 ms
    intervalIDs[socket.id] = setInterval(
      -> socket.emit "ping", time: Date.now()
      PING_INTERVAL
    )

    socket.on "pong", (data) ->

      latencies = allLatencies[socket.id]

      latencies.push timeSince(data.time)/2
      if latencies.length > MAX_NUM_LATENCIES
        latencies.shift()

      calculateTypicalLatency socket.id

      null

    socket.on "disconnect", ->

      clearInterval intervalIDs[socket.id]
      delete allLatencies[socket.id]
      delete typicalLatencies[socket.id]
      delete intervalIDs[socket.id]

      null


module.exports.getTypicalLatency = (id) ->
  typicalLatencies[id] ? null