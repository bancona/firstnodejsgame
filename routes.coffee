module.exports = (app, express) ->

  # Render "index" on request to root
  app.get "/", (req, res) -> res.render "index", title: "Hello World!"